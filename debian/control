Source: virtualenv-clone
Section: python
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-pytest <!nocheck>,
 python3-setuptools,
 python3-virtualenv,
 virtualenv,
Standards-Version: 4.6.2
Homepage: https://github.com/edwardgeorge/virtualenv-clone
Vcs-Git: https://salsa.debian.org/debian/virtualenv-clone.git
Vcs-Browser: https://salsa.debian.org/debian/virtualenv-clone

Package: python3-virtualenv-clone
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Breaks:
 virtualenv-clone (<< 0.3.0-1.2~),
Replaces:
 virtualenv-clone (<< 0.3.0-1.2~),
Conflicts:
 python-virtualenv-clone,
Description: script for cloning a non-relocatable virtualenv (Python3)
 Virtualenv provides a way to make virtualenv's relocatable which could then be
 copied as wanted. However making a virtualenv relocatable this way breaks
 the no-site-packages isolation of the virtualenv as well as other aspects that
 come with relative paths and '/usr/bin/env' shebangs that may be undesirable.
 .
 Also, the .pth and .egg-link rewriting doesn't seem to work as intended. This
 attempts to overcome these issues and provide a way to easily clone an
 existing virtualenv.
 .
 This is the Python3 package.
